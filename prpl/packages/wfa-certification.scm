(define-module (prpl packages wfa-certification)
  #:use-module (guix download)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages admin)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages gtk)
  #:use-module (guix utils))


(define-public libpcap-rpcap
  (package (inherit libpcap)
           (name "libpcap-rpcap")
           (arguments `(#:configure-flags '("--enable-remote") #:tests? #f))))

(define-public tshark
  (package (inherit wireshark)
    (name "tshark")
    (source (origin
              (inherit (package-source wireshark))
              (patches (search-patches "0001-ieee1905-fix-endianness-of-fields.patch"))))

    (arguments
     `(#:configure-flags '("-DBUILD_wireshark=off")
       ;; the following tests are failing, disable them all for now:
       ;; 1 - suite_capture (Failed)
       ;; 29 - suite_release (Failed)
       ;; 32 - suite_unittests (Failed)
       #:tests? #f
       ;; Build process chokes during `validate-runpath' phase.
       ;;
       ;; Errors are like the following:
       ;; "/gnu/store/...wireshark-3.0.0/lib/wireshark/plugins/3.0/epan/ethercat.so:
       ;; error: depends on 'libwireshark.so.12', which cannot be found in
       ;; RUNPATH".  That is, "/gnu/store/...wireshark-3.0.0./lib" doesn't
       ;; belong to RUNPATH.
       ;;
       ;; That’s not a problem in practice because "ethercat.so" is a plugin,
       ;; so it’s dlopen’d by a process that already provides "libwireshark".
       ;; For now, we disable this phase.
       #:validate-runpath? #f))))
(define with-libpcap-rpcap
  (package-input-rewriting `((,libpcap . ,libpcap-rpcap))))

(define tshark-with-rpcap
  (with-libpcap-rpcap tshark))

(define-public tshark-rpcap
  (package (inherit tshark-with-rpcap)
    (name "tshark-rpcap")))
